"use strict";

tlc.services.factory('urlService',urlService);
  function urlService() {
  var urlServiceObj = {};
  urlServiceObj.BASE_URL = '/api';    //this will run for the emulator
  // urlServiceObj.BASE_URL = "http://crimson.goavega.com/api"; // to run the device(ipad... etc) we should use this one
  urlServiceObj.Menu = urlServiceObj.BASE_URL + '/menu';
  urlServiceObj.flowers={};
  return urlServiceObj;
}
