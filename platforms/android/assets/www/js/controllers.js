tlc.controllers.controller('homepagecontroller', function($scope,$stateParams,
  $http,urlService,$state,$rootScope,$ionicLoading) {
     var config = { headers:{
        'X-CLIENT' : "3lpN2uNc8WhIMdMutSBIkkq8wrtB+Rovw399oxVbQck=",
        'Accept': 'text/json'
               }
         }

  //  $ionicLoading.show({
  //    template: '<ion-spinner icon="dots"></ion-spinner>',
  //    hideOnStageChange: true
  //    });

   $http.get(urlService.Menu, config).success(function(data,status) {
     $rootScope.menu=data;
     console.log($rootScope.menu);
     console.log(status);
    //  $ionicLoading.hide();
    }).error(function(err){
  });

$scope.menuClick = function($event){
      var types = angular.element($event.target).attr('menu');
      $state.go('app.flower',{menu:types,isSearch:false,searchKey:'',reload: true});
    }
});

tlc.controllers.controller('flowercontroller', function($scope, $stateParams, $filter,
  $http,urlService,$state,$rootScope,$ionicLoading) {
    var isSearch=$stateParams.isSearch;
    var len=$rootScope.menu.length;
  if(isSearch=='false'){

      $rootScope.titleValue = $stateParams.menu.toUpperCase();
      $rootScope.MenuItems={'items':[]};
        for(i=0; i<len; i++){
           if($rootScope.menu[i].type === $stateParams.menu){
            $rootScope.MenuItems=$rootScope.menu[i];
           }
        }
      }
    });

tlc.controllers.controller('menucontroller', function($scope, $stateParams,
   $filter, $rootScope,  $state){
    $scope.searchVal='';
    $scope.keyPressed=function(e,val){
      if(e.keyCode===13 && val!=""){
        $rootScope.titleValue = "SEARCH RESULTS FOR "+"'"+val.toUpperCase()+"'";
        var len=$rootScope.menu.length;
        var matchedItems;
        $rootScope.MenuItems={'items':[]};
          for(i=0; i<len; i++){
             matchedItems=$filter('filter')($rootScope.menu[i].items,{'name':val});
             $rootScope.MenuItems.items=  $rootScope.MenuItems.items.concat(matchedItems);
             console.log('serached',$rootScope.MenuItems);
           }
       if($state.current.name==='app.playlists'){
          $scope.valName = $state.current.name;
          $state.go('app.flower',{menu:val,isSearch:true});
         }
      }
    }
   });
