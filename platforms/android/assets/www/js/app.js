// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var tlc = {
    controllers: angular.module("tlc.controllers", []),
    services: angular.module("tlc.services", []),
    filters: angular.module("tlc.filters", [])
};
tlc.app = angular.module('tlc', ['ionic', 'tlc.controllers', 'tlc.services'])

.run(function ($ionicPlatform, $rootScope) {
    $rootScope.menu = [];
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.Connection) {
               if(navigator.connection.type == Connection.NONE) {
                   $ionicPopup.confirm({
                       title: "Internet Disconnected",
                       content: "The internet is disconnected on your device."
                   })
                   .then(function(result) {
                       if(!result) {
                           ionic.Platform.exitApp();
                       }
                   });
               }
           }
           
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.backButton.previousTitleText(false);
    $stateProvider

        .state('app', {
        url: '/app',
        cache: false,
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'menucontroller'

    })

    .state('app.flower', {
        cache: false, //ADDED
        url: '/flower/:menu/:isSearch',
        views: {
            'menuContent': {
                templateUrl: 'templates/flower.html',
                controller: 'flowercontroller'
            }
        }
    })



    .state('app.playlists', {
        url: '/home',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/homepage.html',
                controller: 'homepagecontroller'
            }
        }
    })

    .state('app.search', {
        cache: false, //ADDED
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'templates/search.html',
                controller: 'searchcontroller'
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});
