// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var tlc = {
    controllers: angular.module("tlc.controllers", []),
    services: angular.module("tlc.services", [])
};
tlc.app = angular.module('tlc', ['ionic', 'tlc.controllers', 'tlc.services'])

.run(function ($ionicPlatform, $rootScope, $ionicPopup) {

    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        // if(window.Connection) {
        //        if(navigator.connection.type == Connection.NONE) {
        //            $ionicPopup.confirm({
        //                title: "Internet Disconnected",
        //                content: "The internet is disconnected on your device."
        //            })
        //            .then(function(result) {
        //                if(!result) {
        //                     ionic.Platform.exitApp();
        //                 //$state.go($state.current, $stateParams, {reload: true, inherit: false});
        //                }
        //            });
        //        }
        //    }

          $rootScope.menu = [];

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
  $httpProvider.defaults.headers.common = {
      'X-CLIENT': '3lpN2uNc8WhIMdMutSBIkkq8wrtB+Rovw399oxVbQck=',
      'Accept': 'text/json'
    };

    $ionicConfigProvider.backButton.previousTitleText(false);
    $stateProvider

        .state('app', {
        url: '/app',
        cache: false,
        abstract: true,
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'

    })

    .state('app.description', {
        cache: false, //ADDED
        url: '/flower/:menu/:isSearch',
        views: {
            'menuContent': {
                templateUrl: 'templates/menuDesc.html',
                controller: 'menuDescCtrl'
            }
        }
    })

  .state('app.home', {
        url: '/home',
        cache: false,
        views: {
            'menuContent': {
                templateUrl: 'templates/homepage.html',
                controller: 'homepageCtrl'
            }
        }
    });

   // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});
