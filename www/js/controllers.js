  "use strict";

  tlc.controllers.controller('homepageCtrl', function($scope,$http,urlService,$state,$rootScope,$ionicLoading) {


    $ionicLoading.show({template: '<ion-spinner icon="bubbles"></ion-spinner>',
                             hideOnStageChange: true,
                      });

    $http.get(urlService.Menu).success(function(data,status) {
       $rootScope.menu=data;
       var len=$rootScope.menu.length;
       $scope.menuTypes = [];
         for(var i=0; i<len; i++){
            $scope.menuTypes.push($rootScope.menu[i].type);
         }

        function chunk(arr, size) {
           var newArr = [];
           for (var i=0; i<arr.length; i=size+i) {
              newArr.push(arr.slice(i, i+size));
           }
           return newArr;
        }

       $scope.chunkedData = chunk($scope.menuTypes, 3);
       $ionicLoading.hide();
     });

       $scope.menuClick = function(types){
           $state.go('app.description', {menu:types,isSearch:false});
       };
  });

  tlc.controllers.controller('menuDescCtrl', function($scope,$stateParams,$rootScope) {
      var isSearch=$stateParams.isSearch;
      var len=$rootScope.menu.length;
      if(isSearch=='false'){
        $rootScope.titleValue = $stateParams.menu.toUpperCase();
        $rootScope.MenuItems={'items':[]};
          for(var i=0; i<len; i++){
             if($rootScope.menu[i].type === $stateParams.menu){
              $rootScope.MenuItems=$rootScope.menu[i];
              console.log('$rootScope.MenuItems : ', $rootScope.MenuItems  );
             }
          }
      }
  });

  tlc.controllers.controller('searchCtrl', function($scope,$filter,$rootScope,$state){
      $scope.searchVal='';
      $scope.keyPressed=function(e,val){
        if(e.keyCode===13 && val!==""){

          $rootScope.titleValue = "SEARCH RESULTS FOR "+"'"+val.toUpperCase()+"'";
          var len=$rootScope.menu.length;
          var matchedItems;
          $rootScope.MenuItems = {'items':[]};
            for(var i=0; i<len; i++){
               matchedItems=$filter('filter')($rootScope.menu[i].items,{'name':val});
               $rootScope.MenuItems.items=  $rootScope.MenuItems.items.concat(matchedItems);
             }

         if($state.current.name ==='app.home'){
            $state.go('app.description',{menu:val,isSearch:true});
           }
        }
      };
     });
